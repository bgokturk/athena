//dear emacs, this is -*-c++-*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

template<class MC_t, class SYM_t>
LArSymConditionsAlg<MC_t,SYM_t>::LArSymConditionsAlg(const std::string& name, ISvcLocator* pSvcLocator) :
  AthAlgorithm(name, pSvcLocator),
  m_readKey("RampMC"),
  m_mcSymKey("LArMCSym"),
  m_writeKey("RampSym","RampSym"),
  m_condSvc("CondSvc",name)
{
  declareProperty("ReadKey",m_readKey);
  declareProperty("MCSymKey",m_mcSymKey);
  declareProperty("WriteKey",m_writeKey);
}

template<class MC_t, class SYM_t>
LArSymConditionsAlg<MC_t,SYM_t>::~LArSymConditionsAlg() {}


template<class MC_t, class SYM_t>
StatusCode LArSymConditionsAlg<MC_t,SYM_t>::initialize() {

  // CondSvc
  ATH_CHECK( m_condSvc.retrieve() );
  // Read Handles
  ATH_CHECK( m_readKey.initialize() );
  ATH_CHECK( m_mcSymKey.initialize() );
  ATH_CHECK( m_writeKey.initialize() );
  // Register write handle
  if (m_condSvc->regHandle(this, m_writeKey).isFailure()) {
    ATH_MSG_ERROR("unable to register WriteCondHandle " << m_writeKey.fullKey() << " with CondSvc");
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}


template<class MC_t, class SYM_t>
StatusCode LArSymConditionsAlg<MC_t,SYM_t>::execute() {
    
  SG::WriteCondHandle<SYM_t> writeHandle{m_writeKey};
  
  if (writeHandle.isValid()) {
    ATH_MSG_DEBUG("Found valid write handle");
    return StatusCode::SUCCESS;
  }  

  SG::ReadCondHandle<MC_t> readHandle{m_readKey};
  const MC_t* mcClass{*readHandle};

  if (mcClass==nullptr) {
    ATH_MSG_ERROR("Failed to retrieve input object with key " << m_readKey.key());
    return StatusCode::FAILURE;
  }

  SG::ReadCondHandle<LArMCSym> mcSymHdl{m_mcSymKey};
  const LArMCSym* mcSym={*mcSymHdl};

  if (mcSym==nullptr) {
    ATH_MSG_ERROR("Failed to retrieve LArMCSym object with key " << m_mcSymKey.key());
    return StatusCode::FAILURE;
  }



  std::unique_ptr<SYM_t> sym=std::make_unique<SYM_t>(mcSym,mcClass);
  
  // Define validity of the output cond object and record it
  EventIDRange rangeW;
  if(!readHandle.range(rangeW)) {
    ATH_MSG_ERROR("Failed to retrieve validity range for " << readHandle.key());
    return StatusCode::FAILURE;
  }

  if(writeHandle.record(rangeW,sym.release()).isFailure()) {
    ATH_MSG_ERROR("Could not record LArSymCond object with " 
		  << writeHandle.key() 
		  << " with EventRange " << rangeW
		  << " into Conditions Store");
    return StatusCode::FAILURE;
  }
  ATH_MSG_INFO("recorded new " << writeHandle.key() << " with range " << rangeW << " into Conditions Store");

 
  return StatusCode::SUCCESS;
}

